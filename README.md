# JavaKata

The following is a kata Test driven development that 
takes as input a string numbers = '-3,34,fdsd343356/n34' 
and outputs the sum of valid numbers (non-negative, not greater than 1000), 
eg sum = 34+34=68.

For this purpose, a StringCalculator class was created, within which 
methods were created that incrementally approach the aim of the kata:

#  Methods that incrementally complete kata taking as input (String numbers):
* **find** -> look for numbers with Regex:
if at least one number [0-9] is contained return True otherwise returns False.

* **addDigit** -> I add up each digit: 
"1,2" is like "12" and returns 1+2 = 3

*  **addNumber** -> I add up each number:
"12,30" becomes 12 + 30 = 42
to do so I used 2 for loops:
 the first converts the non "-" non digits in "b".
 the second loop iterate throw the new list and check weather the chart is 
a b or the last of the original string if so it checks weather the last char is 
a digit to include it in num and then in sum. Finally reinitialize num to empty string.
Otherwise (if char is a digit) it concatenate char digits to build the number
eg numbers="123,34" -> num = "123" and then num = "34" -> sum = 123+34 = 157

* **add** -> I add up each number (like addNumber) with 1 for loop: Increased control on negaitve num. Put the numbers in 3 Integer ArrayList of: negative, big, in 0-1000 range.

* **addExternalFunction** -> final function to add up numbers. add function exploiting an external class method to wrap if statements that check wheter num is negative, big, in 0-1000 range. 
The NumberListPopulator class needs a ReturningValues object to retunr multiple values:  output = populator.populate(num, sum, negativeNumberList, bigNumberList, numberList);

# In parallel with the implementation of new methods, new tests were also implemented:
In parallel with the implementation of new methods, new tests were also implemented, as the last thing the function is called in the main. 
To test different strings you can change the input of StringCalculator in the main 
or in the tests and then Run 'Main.main()' or Run 'addExternal1()' which is the last implemented test, 
or create new tests from the ones already made.
