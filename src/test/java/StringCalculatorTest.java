import org.example.StringCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringCalculatorTest {
    @Test
    public void compare1() {
        StringCalculator calculatorTests = new StringCalculator();
        int value = calculatorTests.compare(2, 1);
        Assertions.assertEquals(1, value);
    }
    @Test
    public void find1() {
        StringCalculator calculatorTests = new StringCalculator();
        boolean value = calculatorTests.find("1,2,3");
        Assertions.assertEquals(true, value);
    }

    @Test
    public void addDigit1() {
        StringCalculator calculatorTests = new StringCalculator();
        int value = calculatorTests.addDigit("123,123");
        Assertions.assertEquals(12, value);
    }

    @Test
    public void addNumber1() {
        StringCalculator calculatorTests = new StringCalculator();
        int value = calculatorTests.addNumber("123,123");
        Assertions.assertEquals(246, value);
    }

    @Test
    public void addNumberNegative() {
        StringCalculator calculatorTests = new StringCalculator();
        int value = calculatorTests.addNumber("123,-123");
        Assertions.assertEquals(0, value);
    }

    @Test
    public void addNumberNegative2() {
        StringCalculator calculatorTests = new StringCalculator();
        int value = calculatorTests.addNumber("123,-123/n&&12");
        Assertions.assertEquals(12, value);
    }

    @Test
    public void addNumberNegative3() {
        StringCalculator calculatorTests = new StringCalculator();
        int value = calculatorTests.addNumber("123,-123, 12-2");
        Assertions.assertEquals(10, value);
    }

    // ADD = final function
    @Test
    public void add1() {
        StringCalculator calculatorTests = new StringCalculator();
        int value = calculatorTests.add("123,123");
        Assertions.assertEquals(246, value);
    }

    @Test
    public void add2() {
        StringCalculator calculatorTests = new StringCalculator();
        int value = calculatorTests.add("123/n&&12");
        Assertions.assertEquals(135, value);
    }

    @Test
    public void add3() {
        StringCalculator calculatorTests = new StringCalculator();
        int value = calculatorTests.add("1230mmbvh1n2n-,123");
        Assertions.assertEquals(126, value);
    }
    @Test
    public void addNegative() {
        StringCalculator calculatorTests = new StringCalculator();
        int value = calculatorTests.add("123,-123");
        Assertions.assertEquals(123, value);
    }
    @Test
    public void addNegative2() {
        StringCalculator calculatorTests = new StringCalculator();
        int value = calculatorTests.add("-1230mmbvh1n2n-,123,--,-2-1");
        Assertions.assertEquals(126, value);
    }

    @Test
    public void addExternal1() {
        StringCalculator calculatorTests = new StringCalculator();
        int value = calculatorTests.addExternalFunction("1230mmbvh1n2n-,123,--,-2-1");
        Assertions.assertEquals(126, value);
    }
}
