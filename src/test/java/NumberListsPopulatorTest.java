import org.example.NumberListsPopulator;
import org.example.ReturningValues;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


public class NumberListsPopulatorTest {
    @Test
    public void populate1() {
        NumberListsPopulator populatorTests = new NumberListsPopulator();
        ArrayList<Integer> negativeNumberList = new ArrayList<>();
        ArrayList<Integer> bigNumberList = new ArrayList<>();
        ArrayList<Integer> numberList = new ArrayList<>();
        int sum = 0;
        String num = "123";

        ReturningValues list = populatorTests.populate(num, sum, negativeNumberList,
                bigNumberList, numberList);

        String num_exp = "";
        Integer sum_exp = 123;
        ArrayList<Integer> negativeNumberList_exp = new ArrayList<>();
        ArrayList<Integer> bigNumberList_exp = new ArrayList<>();
        ArrayList<Integer> numberList_exp = new ArrayList<>(); // [123];
        numberList_exp.add(123);

        Assertions.assertEquals(num_exp, list.num);
        Assertions.assertEquals(sum_exp, list.sum);
        Assertions.assertArrayEquals(negativeNumberList_exp.toArray(), list.negativeNumberList.toArray());
        Assertions.assertArrayEquals(bigNumberList_exp.toArray(), list.bigNumberList.toArray());
        Assertions.assertArrayEquals(numberList_exp.toArray(), list.numberList.toArray());

    }

    @Test
    public void populate2() {
        NumberListsPopulator populatorTests = new NumberListsPopulator();
        ArrayList<Integer> negativeNumberList = new ArrayList<>();
        ArrayList<Integer> bigNumberList = new ArrayList<>();
        ArrayList<Integer> numberList = new ArrayList<>();
        int sum = 0;
        String num = "123,-2-3,12345";
        ReturningValues list = populatorTests.populate(num, sum, negativeNumberList,
                bigNumberList, numberList);

        String num_exp = "";
        Integer sum_exp = 123;
        ArrayList<Integer> negativeNumberList_exp = new ArrayList<>();
        negativeNumberList_exp.add(-2);
        negativeNumberList_exp.add(-3);
        ArrayList<Integer> bigNumberList_exp = new ArrayList<>();
        negativeNumberList_exp.add(12345);
        ArrayList<Integer> numberList_exp = new ArrayList<>(); // [123];
        numberList_exp.add(123);

        Assertions.assertEquals(num_exp, list.num);
        Assertions.assertEquals(sum_exp, list.sum);
        Assertions.assertArrayEquals(negativeNumberList_exp.toArray(), list.negativeNumberList.toArray());
        Assertions.assertArrayEquals(bigNumberList_exp.toArray(), list.bigNumberList.toArray());
        Assertions.assertArrayEquals(numberList_exp.toArray(), list.numberList.toArray());

    }
}
