package org.example;
import java.util.ArrayList;

public class NumberListsPopulator {

    /* populate is a method that checks the value of the string num.
    Num contains negative numbers too, e.g. '123', '-12', '1005'.
    The populate method converts num to an integer and checks if
    it is a negative number, large and
    if num is between 0 and 1000 it is added to sum.
    Sum is an integer used to count the numbers contained in
    the numbers string, input to the addExternalFunction method,
    e.g. '-124-2,23456niuxbiksa66'.
    * */

    // wrap up ArrayList, string and int variables
    // into the object ReturningValues to return them all

    public ReturningValues populate(String num, Integer sum,
                                   ArrayList<Integer> negativeNumberList,
                                   ArrayList<Integer> bigNumberList,
                                   ArrayList<Integer> numberList) {
        try {
            if (Integer.valueOf(num) < 0) {
                negativeNumberList.add(Integer.valueOf(num));
                num = ""; // ignoro i numeri negativi
            } else if (Integer.valueOf(num) > 1000) {
                bigNumberList.add(Integer.valueOf(num));
                num = ""; // ignoro i numeri grossi
            } else {
                // la stringa finisce con un numero
                sum += Integer.valueOf(num);
                numberList.add(Integer.valueOf(num));
                num = ""; // reinizializzo num
            }
            ReturningValues list = new ReturningValues(num, sum,
                    negativeNumberList, bigNumberList, numberList);
            return list;
        } catch (NumberFormatException e) {
            // Integer.valueOf(num)
            throw new RuntimeException(e);
        }
    }

}
