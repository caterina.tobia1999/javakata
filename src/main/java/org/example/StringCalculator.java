package org.example;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringCalculator {
    /*
    METHODS:
    - compare (int n1, int n2) test example
    methods that incrementally complete kata:
    - find (String numbers)
    - addDigit
    - addNumber
    - add
    - addExternalFunction
    * */
    public int compare(int n1, int n2) {
        if (n1 > n2) return 1;
        return -1;
    }

    // numbers ``""`` or ``"1"`` or ``"1,2"``
    public boolean find(String numbers){
        int sum = 0;
        Pattern pattern = Pattern.compile("[0-9]");
        Matcher matcher = pattern.matcher(numbers);
        boolean matchFound = matcher.find();
        // if matchFound is false return 0 else return the sum
        return matchFound;
    }

    // I add up each digit: "1,2" is like "12" and returns 1+2 = 3
    public int addDigit(String numbers){
        int sum = 0;

        for(char n : numbers.toCharArray()){
            if(Character.isDigit(n)){
                System.out.println(n);
                sum += Character.getNumericValue(n);
                System.out.println(sum);
            }
        }
        return sum;
    }

    public int addNumber(String numbers){
        int sum = 0;
        char [] chars = numbers.toCharArray();
        // System.out.println(chars.length);

        ArrayList<Character> numberList = new ArrayList<>();
        for(int i = 0; i < chars.length; i++){
            if(Character.isDigit(numbers.charAt(i))){
                // System.out.println(chars[i]);
                numberList.add(chars[i]);
            } else if(numbers.charAt(i)=='-') {
                numberList.add(chars[i]);
                // o errore
            }
            else{
                numberList.add('b');
            }
        }

        System.out.println(numberList);
        String num = "";
        // for(int i = 0; i < numberList.size(); i++){
        for(char n : numberList){
            if((n=='b') || (n==numberList.get(numberList.size()-1))) {
                if ((n==numberList.get(numberList.size()-1)) && (Character.isDigit(n))){
                    num += n;
                }
                if(num != "") {
                    System.out.println(num);
                    // sum +=  Character.getNumericValue(num);
                    sum += Integer.valueOf(num);
                    num = "";
                }
            }
            else{
                num += n;
            }
        }

        System.out.println(sum);

        return sum;
    }

    public int add(String numbers) {

        int sum = 0;
        String num = "";
        char[] chars = numbers.toCharArray();

        ArrayList<Integer> negativeNumberList = new ArrayList<>();
        ArrayList<Integer> bigNumberList = new ArrayList<>();
        ArrayList<Integer> numberList = new ArrayList<>();

        for (int i = 0; i < chars.length; i++) {
            if (Character.isDigit(numbers.charAt(i))) {
                num += chars[i];
                if (i == (chars.length - 1)) {
                    if (Integer.valueOf(num) < 0) {
                        negativeNumberList.add(Integer.valueOf(num));
                        num = ""; // ignoro i numeri negativi
                    } else if (Integer.valueOf(num) > 1000) {
                        bigNumberList.add(Integer.valueOf(num));
                        num = ""; // ignoro i numeri grossi
                    } else {
                        // la stringa finisce con un numero
                        sum += Integer.valueOf(num);
                        numberList.add(Integer.valueOf(num));
                    } }
                }
                else if (numbers.charAt(i) == '-') {
                if (i>0){ // otherwise i-1 is Index -1 out of bounds

                    if (Character.isDigit(numbers.charAt(i-1)) && (num != "")){
                        // se PRIMA del "-" ho un numero devo spezzarli: eg "2-2-3" -> "2,-2,-3"
                        if (Integer.valueOf(num) < 0) {
                            negativeNumberList.add(Integer.valueOf(num));
                            num = ""; // ignoro i numeri negativi
                        } else if (Integer.valueOf(num) > 1000) {
                            bigNumberList.add(Integer.valueOf(num));
                            num = ""; // ignoro i numeri grossi
                        } else {
                            // la stringa finisce con un numero
                            sum += Integer.valueOf(num);
                            numberList.add(Integer.valueOf(num));
                            num = "";
                        }
                    }
                }
                    if (Character.isDigit(numbers.charAt(i+1))){
                        // aggiungo il - solo se dopo ho un numero
                        num += chars[i];
                    }
                    // se non ho numeri ne prima ne dopo ignoro i "-"
                } else if (num != ""){
                    if (Integer.valueOf(num) < 0) {
                            negativeNumberList.add(Integer.valueOf(num));
                            num = ""; // ignoro i numeri negativi
                    } else if (Integer.valueOf(num) > 1000) {
                        bigNumberList.add(Integer.valueOf(num));
                        num = ""; // ignoro i numeri grossi
                    } else {
                        sum += Integer.valueOf(num);
                        numberList.add(Integer.valueOf(num));
                        num = "";
                    }
                }
        }

        System.out.println("The Input String: \n \"" + numbers + "\"\n");

        if (negativeNumberList.size() > 0) {
            System.out.println("Only Positive Numbers! \n " +
                    "The following Negative numbers were Ignored: "
                    + negativeNumberList + "\n");
            // this gets caught in the catch block
            // throw new IllegalArgumentException("negatives not allowed: " + negativeNumberList);
        }
        if (bigNumberList.size() > 0) {
            System.out.println("Only Small Numbers! \n " +
                    "The following Big numbers were Ignored: "
                    + bigNumberList + "\n");
        }
        if (numberList.size() > 0) {
            System.out.println("The Sum is: " + sum + "\n " +
                    "Is the result of the addition of the following numbers: "
                    + numberList + "\n");
        } else{
            System.out.println("The Sum is: " + sum +
                    "\n No Numbers were found in the input string! \n" );
        }

        return sum;
    }

    public int addExternalFunction(String numbers) {

        int sum = 0;
        String num = "";
        char[] chars = numbers.toCharArray();

        ArrayList<Integer> negativeNumberList = new ArrayList<>();
        ArrayList<Integer> bigNumberList = new ArrayList<>();
        ArrayList<Integer> numberList = new ArrayList<>();

        NumberListsPopulator populator = new NumberListsPopulator();
        ReturningValues output = new ReturningValues();

        for (int i = 0; i < chars.length; i++) {
            if (Character.isDigit(numbers.charAt(i))) {
                num += chars[i];
                if (i == (chars.length - 1)) {
                    output = populator.populate(num, sum, negativeNumberList,
                            bigNumberList, numberList);

                    num = output.num;
                    sum = output.sum;
                    negativeNumberList = output.negativeNumberList;
                    bigNumberList = output.bigNumberList;
                    numberList = output.numberList;

                }
            }
            else if (numbers.charAt(i) == '-') {
                if (i>0){ // otherwise i-1 is Index -1 out of bounds
                if ((num != "") && Character.isDigit(numbers.charAt(i-1))){
                    // if BEFORE the '-' I have a number I must break them:
                    // eg "2-2-3" -> "2,-2,-3"
                    output = populator.populate(num, sum, negativeNumberList,
                            bigNumberList, numberList);

                    num = output.num;
                    sum = output.sum;
                    negativeNumberList = output.negativeNumberList;
                    bigNumberList = output.bigNumberList;
                    numberList = output.numberList;

                }}
                if (Character.isDigit(numbers.charAt(i+1))){
                    // I add the - only if I have a number after it
                    num += chars[i];
                }
                // if I have no numbers either before or after I ignore the '-',
                // so the else is not needed
            } else if (num != ""){
                output = populator.populate(num, sum, negativeNumberList,
                        bigNumberList, numberList);

                num = output.num;
                sum = output.sum;
                negativeNumberList = output.negativeNumberList;
                bigNumberList = output.bigNumberList;
                numberList = output.numberList;

            }
        }

        System.out.println("The Input String: \n \"" + numbers + "\"\n");

        if (negativeNumberList.size() > 0) {
            System.out.println("Only Positive Numbers! \n " +
                    "The following Negative numbers were Ignored: "
                    + negativeNumberList + "\n");
            // this gets caught in the catch block
            // throw new IllegalArgumentException("negatives not allowed:
            // " + negativeNumberList);
        }
        if (bigNumberList.size() > 0) {
            System.out.println("Only Small Numbers! \n " +
                    "The following Big numbers were Ignored: "
                    + bigNumberList + "\n");
        }
        if (numberList.size() > 0) {
            System.out.println("The Sum is: " + sum + "\n " +
                    "Is the result of the addition of the following numbers: "
                    + numberList + "\n");
        } else{
            System.out.println("The Sum is: " + sum +
                    "\n No Numbers were found in the input string! \n" );
        }

        return sum;
    }
}



    /*
    public void stringManipulation(String numbers) {

        // char at specific index
        char c = numbers.charAt(2);
        System.out.println(c);
        // char sub array
        char[] chars1 = new char[7];
        numbers.getChars(0, 7, chars1, 0);
        System.out.println(chars1);

        int sum=0;
        char[] chars = numbers.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (Character.isDigit(numbers.charAt(i))) {
                System.out.println(numbers.charAt(i));
                sum += Character.getNumericValue(numbers.charAt(i));
                System.out.println(sum);
            }
        }

        if (input < 0) {
            // this gets caught in the catch block
            throw new IllegalArgumentException("Only Positive Numbers & no Letters Please!");
        }
        } catch (IllegarArgumentException e) {
            System.out.println(e.getMessage());
        }
        }*/

