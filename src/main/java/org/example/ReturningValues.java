package org.example;

import java.util.ArrayList;

public class ReturningValues {
    public String num;
    public Integer sum;
    public ArrayList<Integer> negativeNumberList, bigNumberList, numberList;

    public ReturningValues(){
        this.num = "1,2,3";
        this.sum = 0;
        this.negativeNumberList = null;
        this.bigNumberList = null;
        this.numberList = null;
    }
    public ReturningValues(String num, Integer sum,
                           ArrayList<Integer> negativeNumberList,
                           ArrayList<Integer> bigNumberList,
                           ArrayList<Integer> numberList){
        this.num = num;
        this.sum = sum;
        this.negativeNumberList = negativeNumberList;
        this.bigNumberList = bigNumberList;
        this.numberList = numberList;
    }

}
